
package com.smartwashr.driver.models.loginresponse;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import com.smartwashr.driver.models.User;

public class LoginResponse {

    @SerializedName("status")
    @Expose
    private String status;
    @SerializedName("token")
    @Expose
    private String token;
    @SerializedName("user")
    @Expose
    private User user;
    @SerializedName("active_orders")
    @Expose
    private Integer activeorders;

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getToken() {
        return token;
    }

    public void setToken(String token) {
        this.token = token;
    }

    public User getUser() {
        return user;
    }

    public void setUser(User user) {
        this.user = user;
    }

    public Integer getActiveorders() {
        return activeorders;
    }

    public void setActiveorders(Integer activeorders) {
        this.activeorders = activeorders;
    }
}
