package com.smartwashr.driver.activities;

import android.Manifest;
import android.content.Intent;
import android.os.Bundle;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.smartwashr.driver.BadgeIntentService;
import com.smartwashr.driver.HomeFragment;
import com.smartwashr.driver.R;
import com.smartwashr.driver.utils.Constants;
import com.smartwashr.driver.utils.PermissionManager;
import com.smartwashr.driver.utils.SessionManager;

import me.leolin.shortcutbadger.ShortcutBadger;

public class HomeActivity extends AppCompatActivity
        implements View.OnClickListener {

    private FragmentManager fragmentManager;
    private HomeFragment homeFragment;
    private SessionManager sessionManager;
    private PermissionManager permissionsManager;
    private Toolbar toolbar;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_home);
        toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        if (getIntent().getStringExtra("removeBadge") != null) {
            ShortcutBadger.removeCount(HomeActivity.this);
        }
        int badgeCount = 0;
        try {
            if (sessionManager.getInt(Constants.BADGECOUNT) != 0)
                badgeCount = sessionManager.getInt(Constants.BADGECOUNT);
        } catch (NumberFormatException e) {
            Toast.makeText(getApplicationContext(), "Error input", Toast.LENGTH_SHORT).show();
        }

        sessionManager = new SessionManager(HomeActivity.this);
        sessionManager.createLoginSession();

        fragmentManager = getSupportFragmentManager();
        homeFragment = new HomeFragment();
        setDefaultFragment(homeFragment);

        permissionsManager = PermissionManager.getInstance(HomeActivity.this);
        permissionsManager.getPermissionifNotAvailble(new String[]{android.Manifest.permission.ACCESS_NETWORK_STATE
                , Manifest.permission.INTERNET
                , Manifest.permission.ACCESS_FINE_LOCATION
                , Manifest.permission.ACCESS_COARSE_LOCATION}, 111);


        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(
                this, drawer, toolbar, R.string.navigation_drawer_open, R.string.navigation_drawer_close);
        drawer.setDrawerListener(toggle);
        toggle.syncState();

        NavigationView navigationView = (NavigationView) findViewById(R.id.nav_view);
        setClickListeners();
        startService(
                new Intent(HomeActivity.this, BadgeIntentService.class).putExtra("badgeCount", badgeCount)
        );
    }

    void setClickListeners() {
        findViewById(R.id.home).setOnClickListener(this);
        findViewById(R.id.order_history).setOnClickListener(this);
        findViewById(R.id.my_location).setOnClickListener(this);
        findViewById(R.id.logout).setOnClickListener(this);
        findViewById(R.id.back_arrow).setOnClickListener(this);
    }

    public void setDefaultFragment(Fragment fragment) {
        if (fragmentManager.getBackStackEntryCount() < 1) {
            Log.d("fragment", "set Default_  Count:" + fragmentManager.getBackStackEntryCount());
            fragmentManager = getSupportFragmentManager();
            fragmentManager.beginTransaction()
                    .add(R.id.mainframe, fragment)
                    .addToBackStack(null)
                    .commit();
        }
    }


    public void openFragment(final Fragment fragment) {
        Log.d("fragment", "Count: " + fragmentManager.getBackStackEntryCount());
        runOnUiThread(new Runnable() {
            @Override
            public void run() {
                FragmentTransaction transaction = fragmentManager.beginTransaction();
                if (fragmentManager.getBackStackEntryCount() > 1)
                    fragmentManager.popBackStack();
                transaction.add(R.id.mainframe, fragment);
                transaction.addToBackStack(null);
                transaction.commit();
            }
        });
    }

    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (getSupportFragmentManager().getBackStackEntryCount() == 1) {
            finishAffinity();
        } else {
            super.onBackPressed();
            toolbar.setTitle("Home");
        }
    }

    void closeDrawer() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
    }

    @Override
    public void onClick(View v) {
        switch (v.getId()) {
            case R.id.home:
                if (fragmentManager.getBackStackEntryCount() == 1) {
                    closeDrawer();
                } else {
                    fragmentManager.popBackStack();
                    closeDrawer();
                }
                toolbar.setTitle("Home");
                closeDrawer();
                break;
            case R.id.order_history:
                closeDrawer();
                break;
            case R.id.logout:
                sessionManager.clearSession();
                startActivity(new Intent(HomeActivity.this, Login.class));
                finish();
                closeDrawer();
                break;
            case R.id.back_arrow:
                closeDrawer();
                break;
        }
    }
}
